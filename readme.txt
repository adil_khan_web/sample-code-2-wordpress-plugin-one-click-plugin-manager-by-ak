=== AK Wordpress OneClick Plugin Manager  ===
Contributors: Adil Khan
Tags: Plugin Manager


Integrates the `ak_plugin_update_manager` Plugin into your WordPress install.

== Description ==

This plugin allowes you to manage & upgrade of existing/new plugins.

== Installation ==

1. Upload the `ak_plugin_update_manager` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Changelog ==

= 0.0.1 =
This is the first release of the plugin.