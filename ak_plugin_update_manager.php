<?php
/**
 * Plugin Name: AK Plugins Update Manager
 * Plugin URI: http://wp.sunderland.ac.uk/webteam/the-web-team/
 * Description: This plugin allowes you to manage & Update of existing/New plugins.
 * Version: 0.0.1
 * Latest Version: 0.0.1
 * Author: Adil Khan
 * Author URI: http://wp.sunderland.ac.uk/webteam/the-web-team/
 */

defined( 'ABSPATH' ) or die( 'Plugin file cannot be accessed directly.' );
define( 'AK_PLUGINUPDATE_MANAGER__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
register_activation_hook( __FILE__, array( 'ak_plugin_update_manager', 'plugin_activation' ) );
register_deactivation_hook( __FILE__, array( 'ak_plugin_update_manager', 'plugin_deactivation' ) );

require_once( AK_PLUGINUPDATE_MANAGER__PLUGIN_DIR . 'class.ak_plugin_update_manager.php' );

