<?php


if ( ! class_exists( 'ak_plugin_update_manager' ) ) {
	class ak_plugin_update_manager
	{


		public function __construct()
		{
		
if ( is_admin() ) {
	
if ( !is_multisite() ) {add_action('admin_menu', 'ak_plugin_update_manager::ak_plugin_update_manager_custom_admin_menu');}
			if ( is_multisite() ) {
			add_action('network_admin_menu', 'ak_plugin_update_manager::ak_plugin_update_manager_custom_admin_menu');
			}
			
		}
	}

		public static function ak_plugin_update_manager_custom_admin_menu() {

	$page = add_plugins_page( 'AK-Manage Plugin Updates', 'AK-Manage Plugin Updates',
				 'manage_options', 'ak_plugin_update_manager', 'ak_plugin_update_manager::ak_plugin_update_manager_options_page' );

}


public static function ak_plugin_update_manager_options_page() {
 require_once(AK_PLUGINUPDATE_MANAGER__PLUGIN_DIR."edit.php");
} 

	

	}
	new ak_plugin_update_manager;
}
